# README #

Module installation instructions:

1) Create a folder named WeakEncryption.
2) Download this repository to that folder.
3) Move the WeakEncryption folder to either one of the following folders:
	a) User Specific PowerShell Module Folder (keep reading to find out where this is.)
	b) PowerShell Module Folder for All Users (keep reading to find out where this is.)

To find out where your PowerShell Module folders are, you can run this command in PowerShell:

	cls; $env:psmodulepath -split ";" | ForEach-Object {Echo "$($_)$([Environment]::newline)"}

TIP: Usually the user-specific folder path looks something like this:

	C:\Users\username\WindowsPowerShell\Modules

TIP: Usually the folder path for all users looks something like this:
	
	C:\Program Files\WindowsPowerShell\Modules
	
TIP: After copying this module to the correct folder, it should look something like this:
	
	C:\Users\username\WindowsPowerShell\Modules\WeakEncryption (there will be multiple files in this folder)
	or
	C:\Program Files\WindowsPowerShell\Modules\WeakEncryption (there will be multiple files in this folder)

Here's a link to the official module installation instructions from Microsoft (you won't need this if the directions above are helpful enough.

	https://msdn.microsoft.com/en-us/library/dd878350(v=vs.85).aspx#Anchor_1

