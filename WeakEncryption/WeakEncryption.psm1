function Get-EncryptedString(

    [Parameter(Mandatory=$True)]
    [string]$Plaintext,

    [Parameter(Mandatory=$False)]
    [int]$Key = 3

) {

    If ($Key -lt 0 -or $Key -gt 26) {
        Write-Error `
            -Message "Key out of range. -Key parameter must be an integer from 0 to 26."`
            -RecommendedAction "Enter a number from 0 to 26."`
            -Category InvalidArgument `
            -ErrorAction Stop
    }

    [int]$_asciiUpperCaseA = 65
    [int]$_asciiUpperCaseZ = 90

    [int]$_asciiLowerCaseA = 97
    [int]$_asciiLowerCaseZ = 122

    [int]$_asciiUpperCaseOffset = $_asciiUpperCaseZ - $Key
    [int]$_asciiLowerCaseOffset = $_asciiLowerCaseZ - $Key

    $_outputString = ""
    ForEach(

        $x in $Plaintext.ToCharArray()

    ){

        If (
     
            ($x -gt $_asciiUpperCaseOffset -and $x -le $_asciiUpperCaseZ) -or 
            ($x -gt $_asciiLowerCaseOffset -and $x -le $_asciiLowerCaseZ)

        ) {

            $_asciiValue = [byte][char]$x
            $_asciiValueUpdated = $_asciiValue - 26 + $Key
            $_outputString += [char]$_asciiValueUpdated
        
        } elseif (

            ($x -ge $_asciiUpperCaseA -and $x -le $_asciiUpperCaseOffset) -or 
            ($x -ge $_asciiLowerCaseA -and $x -le $_asciiLowerCaseOffset)
    
        ) {

            $_asciiValue = [byte][char]$x
            $_asciiValueUpdated = $_asciiValue + $Key
            $_outputString += [char]$_asciiValueUpdated

        } else {

            $_outputString += $x

        }
    }

    Return $_outputString

<#
    .SYNOPSIS
    Encrypts a string with a weak cipher

    .DESCRIPTION
    Encrypts a string using extremely simple monoalphabetic substition.
    
    This is a very weak encryption method based on Caesar's shift cipher.
    
    The key is an integer from 0 to 26 which is the number of letters to shift to the right for every alphabetic character.
    
    It does not affect punctuation, numbers, or any other characters - letters only.

    .PARAMETER Key
    Specifies an integer as the cipher key which is the number of alphabetical characters to shift. The default value is 3 (which is what Caesar used.)

    .PARAMETER Plaintext
    Specifies the string or message you intend to encrypt.

    .EXAMPLE
    C:\PS> Get-EncryptedString "This is a message."
    Wklv lv d phvvdjh.

    .EXAMPLE
    C:\PS> Get-EncryptedString "Another message." 10
    Kxydrob wocckqo.

    .EXAMPLE
    C:\PS> Get-EncryptedString -PlainText "One last example." -Key 25
    Nmd kzrs dwzlokd.
#>
}